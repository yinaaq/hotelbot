config = {
	"country_code": "IN",
	"urls": {
		"suggestions": {
			"city": "https://www.makemytrip.com/new_hlp/hotelsTypeahead/?&term={0}"
		},
		"data": {
			"firstSearch": "https://hotelz.makemytrip.com/makemytrip/site/hotels/search?checkin={}&checkout={}&city={}&country={}&area=&roomStayQualifier=1e0e&searchText={}",
			"jsonSearch": "https://hotelz.makemytrip.com/makemytrip/site/hotels/{}",
			"pageSearch": "https://hotelz.makemytrip.com/makemytrip/site/hotels/search/page?session_cId={}&pageNum={}&city={}&country={}&checkin={}&checkout={}&searchText={}&region=&area=&roomStayQualifier=1e0e"
		}
	},
	"pattern": {
		"sessionid_input": '(input\sid=\"session_cid\"\sname=\"session_cId\"\svalue=\")([0-9]*)(\")',
		"json_search_url": 'search\/searchWithJson.*\Prxy=false'

	},
	"max_retry_count": 10
}



