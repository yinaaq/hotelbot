import urllib2, cookielib
import os

cookieFile = "makemytrip.txt"

class WebClient:
	_cookieJar = None
	_opener = None

	def __init__(self):
		self._cookieJar = cookielib.MozillaCookieJar()
		self._opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(self._cookieJar))

	def open(self, url):
		request = urllib2.Request(url)
		if os.path.exists(cookieFile):
			self._cookieJar.load(cookieFile)

		response = self._opener.open(request)
		self._cookieJar.save(cookieFile)
		return response
