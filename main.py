#! /usr/bin/python
import urllib2
import json
import hbdb
import sys
import cookielib
import time
import urllib

from hbconfig import config
from hbclient import WebClient
import hbutils

import re

from datetime import datetime
from datetime import timedelta

searchKeyword = ""
countryCode = config["country_code"]

if len(sys.argv) == 1:
	print "City search keyword expected"
	sys.exit(1)
elif len(sys.argv) == 2:
	searchKeyword = sys.argv[1]
	if len(searchKeyword) < 3:
		print "Search keyword must be atleast 3 characters long"
		sys.exit(1)

if len(sys.argv) == 3:
	countryCode = sys.argv[2]

url = config['urls']['suggestions']['city'] .format(searchKeyword)
cityRequest = urllib2.urlopen(url)
data = cityRequest.read()
try:
	data = json.loads(data, 'latin-1')
except Exception as ex:
	print "Failed to decode the json output."
	print data
	print ex
	sys.exit(2)

areas = []
for doc in data['response']['docs']:
	if isinstance(doc, dict):
		if doc["country_code"] == countryCode:
			area = {
			"name": doc["city"],
			"cityCode": doc["hotel"],
			"label": doc["value"]
			}
			
			dbitem = hbdb.areas.find_one({ "name": area["name"], "cityCode": area["cityCode"] })
			if dbitem is None:
				result = hbdb.areas.insert_one(area)
				area["_id"] = result.inserted_id
			else:
				area = dbitem
			areas.append(area)


d1 = datetime.now() + timedelta(1)
d2 = datetime.now() + timedelta(2)
checkIn = d1.strftime("%m%d%Y")
checkOut = d2.strftime("%m%d%Y")
webClient = WebClient()

for area in areas:
	searchUrl = ''

	try:		
		area_url = config["urls"]["data"]["firstSearch"].format(checkIn, checkOut,
			area["cityCode"], 
			countryCode, 
			urllib.quote(area["name"]))

		response = webClient.open(area_url)
		if response is None:
			continue
		
		data = response.read()
		match = re.search(config["pattern"]["json_search_url"], data)
		searchUrl = config["urls"]["data"]["jsonSearch"].format(match.group(0))

		match = re.search(config["pattern"]["sessionid_input"], data)
		sessionid = match.group(2)

		searchUrl = searchUrl.replace(' ', '%20')
		jsonResponse = webClient.open(searchUrl)

		if jsonResponse is None:
			continue

		jsonString = jsonResponse.read()
		result = json.loads(jsonString, 'latin-1')
		hotelsList = result["searchResponseDTO"]["hotelsList"]

		for hotel in hotelsList:
			hbutils.update_hotel(hotel)

		hotelCount = 0
		for pageIndex in range(2, 15):
			try:			
				pageUrl = config["urls"]["data"]["pageSearch"]
				pageUrl = pageUrl.format(sessionid, pageIndex, 
					area["cityCode"], 
					countryCode, checkIn, checkOut, 
					urllib.quote(area["name"]))
				response = webClient.open(pageUrl)
				jsonString = response.read()
				result = json.loads(jsonString, 'latin-1')
				hotelsList = result["hotelsList"]
				hotelCount = 0;
				for hotel in hotelsList:
					hotelCount += 1
					hbutils.update_hotel(hotel)
			except:
				break
	except Exception as ex:
		print ex
		print searchUrl


