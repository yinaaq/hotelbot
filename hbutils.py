import hbdb



def update_hotel(hotel):
	item = {
		"mmt_reference": hotel["id"],
		"name": hotel["name"],
		"address": hotel["address"],
		"geoLocation": hotel["geoLocation"]
	}

	keys = hotel.keys()
	if "mainImgUrl" in keys: item["mmt_main_img_url"] = hotel["mainImgUrl"].replace("//", "https://")
	if "punchline" in keys: item["description"] = hotel["punchline"]
	
	dbitem = hbdb.hotels.find_one({ "mmt_reference": item["mmt_reference"] })
	if dbitem is None:
		hbdb.hotels.insert_one(item)
	else:
		hbdb.hotels.replace_one({ "_id": dbitem["_id"] }, item )

	update_hotel.totalCount += 1
	print "{0}\t".format(update_hotel.totalCount) + item["name"]
	print "\t" + item["address"]["line1"]
	print "\t" + item["address"]["city"] + ", " + item["address"]["state"] + "\n"


update_hotel.totalCount = 0